# [Linux VR Adventures Wiki](https://lvra.gitlab.io)

The Linux VR knowledge base!

---

## Requirements

You will need to install [Hugo extended](https://gohugo.io/installation/linux/).

### Cloning and submodules

You'll need to clone this repo with submodules or init your submodules after the fact.

#### Clone with submodules

```bash
git clone https://gitlab.com/LVRA/wiki --recurse-submodules
```

#### Init submodules in existing clone

```bash
git submodule init
git submodule update
```

## Running

You can run a live server with

```bash
hugo serve
```

## Building

```bash
hugo
```

# Editing and contributing

You can create pages and sections in `content/docs`.

For more info on what's possible you can reference the [Hugo Book Theme](https://github.com/alex-shpak/hugo-book) readme.
