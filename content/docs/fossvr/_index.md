---
weight: 200
title: FOSS VR
---

# FOSS VR

If you want an alternative to SteamVR, there are a few options you can use, depending on your hardware:

- For **PC VR headsets** you can use [Monado](/docs/fossvr/monado/)
- For **standalone headsets** you can use [WiVRn](/docs/fossvr/wivrn/)

In either case, here are some related projects you might want to check out:

- [OpenComposite](/docs/fossvr/opencomposite/) allows you to run OpenVR games
- [Envision](/docs/fossvr/envision/) is a GUI to setup and run either Monado or WiVRn
- [Stardust XR](/docs/fossvr/stardust/) is an XR environment to run 2D and (eventually) 3D apps
- [LÖVR](/docs/fossvr/lovr/) An open source framework for rapidly building immersive 3D experiences.
- [DEMoCap](/docs/fossvr/democap/) An open source motion capture tool using VR hardware.